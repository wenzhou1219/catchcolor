#include <Windows.h>
#include <WindowsX.h>
#include <tchar.h>

#include "resource.h"
#include "SettingCatchProc.h"
#include "config.h"
#include "hotkey.h"

BOOL CALLBACK SettingCatchProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	TCHAR szInt[10];

	switch(message)
	{
	case WM_INITDIALOG:
        {
			//使用全局参数设置控件初始状态
			ComboBox_InsertString(GetDlgItem(hDlg, IDC_VIEW1),0, TEXT("1"));
			ComboBox_InsertString(GetDlgItem(hDlg, IDC_VIEW1),1, TEXT("3"));
			ComboBox_InsertString(GetDlgItem(hDlg, IDC_VIEW1),2, TEXT("6"));
			ComboBox_InsertString(GetDlgItem(hDlg, IDC_VIEW1),3, TEXT("9"));
			ComboBox_InsertString(GetDlgItem(hDlg, IDC_VIEW1),4, TEXT("12"));
			_itot(GetConfig(TEXT("View1Ratio")), szInt, 10);
			ComboBox_SelectString(GetDlgItem(hDlg, IDC_VIEW1), 0, szInt);

			ComboBox_InsertString(GetDlgItem(hDlg, IDC_VIEW2),0, TEXT("1"));
			ComboBox_InsertString(GetDlgItem(hDlg, IDC_VIEW2),1, TEXT("3"));
			ComboBox_InsertString(GetDlgItem(hDlg, IDC_VIEW2),2, TEXT("6"));
			ComboBox_InsertString(GetDlgItem(hDlg, IDC_VIEW2),3, TEXT("9"));
			ComboBox_InsertString(GetDlgItem(hDlg, IDC_VIEW2),4, TEXT("12"));
			_itot(GetConfig(TEXT("View2Ratio")), szInt, 10);
			ComboBox_SelectString(GetDlgItem(hDlg, IDC_VIEW2), 0, szInt);

			ComboBox_InsertString(GetDlgItem(hDlg, IDC_COLORTYPE),0, TEXT("RGB"));
			ComboBox_InsertString(GetDlgItem(hDlg, IDC_COLORTYPE),1, TEXT("HTML"));
			ComboBox_InsertString(GetDlgItem(hDlg, IDC_COLORTYPE),2, TEXT("HEX"));
			ComboBox_SetCurSel(GetDlgItem(hDlg, IDC_COLORTYPE), (INT)GetConfig(TEXT("ColorType")));

			Button_SetCheck(GetDlgItem(hDlg, IDCK_MOVE), GetConfig(TEXT("IsMove")) ? BST_CHECKED : BST_UNCHECKED);
			Button_SetCheck(GetDlgItem(hDlg, IDCK_FORGROUND), GetConfig(TEXT("IsForground")) ? BST_CHECKED : BST_UNCHECKED);
	
			ShowHotkey(GetDlgItem(hDlg, IDHT_CATCH_COLOR), GetConfig(TEXT("ClrModifier")), GetConfig(TEXT("ClrKey")));
        }
		return (TRUE);

	//对话框和控件背景设为白色
	case WM_CTLCOLORDLG:
	case WM_CTLCOLORBTN:
	case WM_CTLCOLORSTATIC:
		return GetStockObject(WHITE_BRUSH);
	}

	return (FALSE);
}