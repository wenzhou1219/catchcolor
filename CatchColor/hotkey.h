#ifndef HOTKEY_H_H_H
#define HOTKEY_H_H_H

extern WORD g_hotkeyClrId;
extern WORD g_hotkeySettingId;

INT SetHotkey(HWND hwnd, UINT nModifier, UINT nKey, PTSTR szAtomName);
BOOL CancelHotkey(HWND hwnd, WORD hotkeyId);
void GetHotkey(HWND hHotkey, UINT *nModifier, UINT *nKey);
void ShowHotkey(HWND hHotkey, UINT nModifier, UINT nKey);

#endif