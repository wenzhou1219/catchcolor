#ifndef SETTING_DLG_PROC_H_H
#define SETTING_DLG_PROC_H_H

BOOL CALLBACK SettingDlgProc (HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
void DownDlgCreate(HWND hDlg);
void DownDlgSelect(UINT nIndex);

#endif