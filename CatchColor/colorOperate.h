#ifndef COLOR_OPERATE_H_H
#define COLOR_OPERATE_H_H

/************************************************************************/
/* 类型定义		                                                        */
/************************************************************************/
#define MAX_COUNT 255							//最大取色历史数

typedef enum tagCOLORTYPE						//颜色类型
{
	COLORTYPE_RGB=0,
	COLORTYPE_HTML,
	COLORTYPE_HEX,
	COLORTYPE_CMYK
}COLORTYPE;

typedef struct tagCOLORNODE						//取色历史数组类型
{
	COLORTYPE clrType;
	BYTE byteRed;
	BYTE byteGreen;
	BYTE byteBlue;
}COLORNODE;

BOOL PushClrNode(const COLORNODE clrNode, HWND hList);
BOOL PushClrTypeAndRGB(const COLORTYPE clrType, const COLORREF clrRef, HWND hList);
BOOL ClearClrList(HWND hList);
COLORNODE GetClrNode(const UINT nIndex);
void GetClrNodeText(COLORNODE clrNode, TCHAR szColor[20]);
void GetClrTypeAndRGBText(COLORTYPE clrType, COLORREF clrRGB, TCHAR szColor[20]);
void CopyClrToClipBoard(HWND hDlg, TCHAR szClr[20]);
BOOL ExportColorHistory(LPCTSTR szClrFile);
BOOL ImportColorHistory(LPCTSTR szClrFile, HWND hList);

#endif