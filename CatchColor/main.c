#include <windows.h>
#include <commctrl.h>
#include <tchar.h>

#include "resource.h"
#include "SettingDlgProc.h"
#include "config.h"
#include "trayicon.h"
#include "hotkey.h"
#include "autorun.h"

#pragma comment(lib,"ComCtl32.lib")

/*确保生成的是VS2008风格的对话框*/
#if defined _M_IX86
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='x86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_IA64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='ia64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_X64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='amd64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#else
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif

//定义GUID以辅助单实例启动
#define ONE_INSTANCE_GUID TEXT("0xc5e2f496, 0xfc6d, 0x4882, 0xac, 0x6a, 0xfd, 0xf4, 0xde, 0xb1, 0xac, 0x5b")

int WINAPI WinMain (HINSTANCE hInstance, HINSTANCE hPrevInstance, PSTR szCmdLine, int iCmdShow)
{
	HWND hwndSettingDlg;
	MSG msg;
	INITCOMMONCONTROLSEX InitCtrls;
	WIN32_FIND_DATA fd;

	TCHAR szDrive[10] ;								//可执行文件盘符
	TCHAR szDir[256] ;								//可执行文件目录
	TCHAR szFilename[64] ;							//可执行文件文件名
	TCHAR szExt[10] ;								//可执行文件文件后缀

	HANDLE hOneInstance;							//应用程序单实例句柄

	// 如果一个运行在 Windows XP 上的应用程序清单指定要
	// 使用 ComCtl32.dll 版本 6 或更高版本来启用可视化方式，
	//则需要 InitCommonControlsEx()。否则，将无法创建窗口。
	InitCtrls.dwSize = sizeof(InitCtrls);
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	/************************************************************************/
	/*应用程序单实例化                                                      */
	/************************************************************************/
	hOneInstance = CreateMutex(NULL, FALSE, ONE_INSTANCE_GUID);//handle为声明的HANDLE类型的全局变量  
	if(GetLastError()==ERROR_ALREADY_EXISTS)  
	{
		MessageBox(NULL, TEXT("应用程序已经在运行!"), TEXT("警告"), MB_OK | MB_ICONWARNING);  
		return 0;  
	}

	/************************************************************************/
	/*获得当前可执行文件和配置文件路径                                      */
	/************************************************************************/
	GetModuleFileName(NULL, g_szExePath, MAX_PATH) ;
	_tsplitpath(g_szExePath, szDrive, szDir, szFilename, szExt) ;				//分割全路径	
	wsprintf(g_szConfigPath, TEXT("%s%ssoftware.ini"), szDrive, szDir) ;		//得到软件配置文件路径	

	//保证配置文件存在和路径正确
	if (INVALID_HANDLE_VALUE  == FindFirstFile(g_szConfigPath, &fd))
	{
		MessageBox(NULL, TEXT("程序配置文件丢失!"), TEXT("警告"), MB_OK | MB_ICONWARNING);
	}

	/************************************************************************/
	/* 读入软件配置参数                                                     */
	/************************************************************************/
	ReadConfig(g_szConfigPath);

	/************************************************************************/
	/*进行开机自动启动初始化设置                                            */
	/************************************************************************/
	if (TRUE == GetConfig(TEXT("IsAutoRun")))
	{
		SetAutoRun(TEXT("JimWen-CatchColor"), g_szExePath);
	}
	else
	{
		CancelAutoRun(TEXT("JimWen-CatchColor"));
	}

	/************************************************************************/
	/* 创建非模态对话框                                                     */
	/************************************************************************/
	hwndSettingDlg = CreateDialog(hInstance, MAKEINTRESOURCE(IDD_SETTING), NULL, SettingDlgProc);

	/************************************************************************/
	/*进行开机软件显示和托盘话初始化设置                                    */
	/************************************************************************/
	if (TRUE == GetConfig(TEXT("IsHide")))
	{
		ShowWindow(hwndSettingDlg, SW_HIDE);
	}
	else
	{
		ToTray(hwndSettingDlg, IDI_MAINICON);
		ShowWindow(hwndSettingDlg, SW_NORMAL);
	}

	/************************************************************************/
	/*设置各个热键						                                    */
	/************************************************************************/
	g_hotkeyClrId =  SetHotkey(hwndSettingDlg, 
							   GetConfig(TEXT("ClrModifier")), 
							   GetConfig(TEXT("ClrKey")), 
							   TEXT("JimWen-ClrHotkey"));
	g_hotkeySettingId =  SetHotkey(hwndSettingDlg, 
									GetConfig(TEXT("SettingModifier")), 
									GetConfig(TEXT("SettingKey")), 
									TEXT("JimWen-SettingHotkey"));

	if (-1 == g_hotkeyClrId)
	{
		MessageBox(hwndSettingDlg, TEXT("取色热键组合已被占用，请重新设置"), TEXT("警告"), MB_OK | MB_ICONWARNING);
	}

	if (-1 == g_hotkeySettingId)
	{
		MessageBox(hwndSettingDlg, TEXT("设置热键组合已被占用，请重新设置"), TEXT("警告"), MB_OK | MB_ICONWARNING);
	}
	
	/************************************************************************/
	/*直接开启消息循环                                                      */
	/************************************************************************/
    while (GetMessage (&msg, NULL, 0, 0))
    {
		if(hwndSettingDlg==0 || !IsDialogMessage(hwndSettingDlg, &msg))
		{
			TranslateMessage (&msg);
			DispatchMessage (&msg);
		}
    }

	/************************************************************************/
	/*退出程序前清理工作                                                    */
	/************************************************************************/
	CancelHotkey(hwndSettingDlg, g_hotkeyClrId);
	CancelHotkey(hwndSettingDlg, g_hotkeySettingId);
	DeleteTray(hwndSettingDlg);
	CloseHandle(hOneInstance);
    return msg.wParam;
}