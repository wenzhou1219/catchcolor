#include <Windows.h>
#include <winbase.h>

#include "resource.h"
#include "ColorDlgProc.h"
#include "config.h"
#include "colorOperate.h"

static RECT DlgRect;

BOOL CALLBACK  ColorDlgProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	static HWND		hwndView1, hwndView2, hwndColor;
	static RECT		wndRect, viewRect1, viewRect2, colorRect;
	static INT		cxScreen, cyScreen;	
	static BOOL		bFirstShow=TRUE;

	POINT			curPt, wndPt;		
	HDC				hdcScreen, hdcView1, hdcView2, hdcColor;
	RECT			catchRect;
	UINT			nViewRatio1, nViewRatio2;
	TCHAR			szBuffer[20];
	COLORREF		clr;

	switch(message)
	{
	case WM_INITDIALOG:
        {
			//获得相关信息
			GetWindowRect(hDlg, &wndRect);
			cxScreen = GetSystemMetrics(SM_CXSCREEN);
			cyScreen = GetSystemMetrics(SM_CYSCREEN);

			hwndView1 = GetDlgItem(hDlg, IDP_VIEW1);
			hwndView2 = GetDlgItem(hDlg, IDP_VIEW2);
			hwndColor = GetDlgItem(hDlg, IDP_COLOR);

			GetWindowRect(hwndView1, &viewRect1);
			GetWindowRect(hwndView2, &viewRect2);
			GetWindowRect(hwndColor, &colorRect);

			//设置窗口为悬浮半透明窗口,注意这里不要加上VISIBLE属性，否则窗口立即显示
			SetWindowToSuspend(hDlg);

			//获取当前鼠标位置以将窗口初始位置置为鼠标附近
			GetCursorPos(&curPt);

			wndPt.x = curPt.x + CURSOR_WINDOW_DIS_X;
			wndPt.y = curPt.y + CURSOR_WINDOW_DIS_Y;

			if(curPt.y + CURSOR_WINDOW_DIS_Y + wndRect.bottom - wndRect.top >= cyScreen)
			{
				wndPt.y = curPt.y - CURSOR_WINDOW_DIS_Y - (wndRect.bottom - wndRect.top);
			}
			if(curPt.x + CURSOR_WINDOW_DIS_X + wndRect.right - wndRect.left >= cxScreen)
			{
				wndPt.x = curPt.x - CURSOR_WINDOW_DIS_X - (wndRect.right - wndRect.left);
			}

			//设置窗口是否总在最前，将窗口初始位置置为鼠标附近
			if (FALSE == GetConfig(TEXT("IsForground")))
			{
				SetWindowPos(hDlg, HWND_NOTOPMOST, wndPt.x, wndPt.y, 0, 0, SWP_NOSIZE);
			}
			else
			{
				SetWindowPos(hDlg, HWND_TOPMOST, wndPt.x, wndPt.y, 0, 0,  SWP_NOSIZE);
			}
				
			//开启定时跟踪
			SetTimer(hDlg, UPDATE_COLOR_TIMER, 100, NULL);
        }
		return (TRUE);

		//更改背景颜色
	case WM_CTLCOLORDLG:
	case WM_CTLCOLORBTN:
	case WM_CTLCOLORSTATIC:
		return GetStockObject(WHITE_BRUSH);

	case WM_CLOSE:
        {
			KillTimer(hDlg, UPDATE_COLOR_TIMER);

            EndDialog(hDlg,0);
        }
		return (TRUE);

	case WM_LBUTTONDOWN:
		if (FALSE == GetConfig(TEXT("IsMove")))
		{
			SendMessage(hDlg, WM_NCLBUTTONDOWN, HTCAPTION, 0);//左键拖动窗口
		}
		return (TRUE);

	case WM_TIMER:
		//获取当前鼠标位置
		GetCursorPos(&curPt);

		//显示当前位置
		wsprintf(szBuffer, TEXT("%d,%d"), curPt.x, curPt.y);
		SetWindowText(GetDlgItem(hDlg, IDS_POS), szBuffer);

		//获取当前放缩比
		nViewRatio1 = GetConfig(TEXT("View1Ratio"));
		nViewRatio2 = GetConfig(TEXT("View2Ratio"));

		/************************************************************************/
		/*窗口跟随移动                                                          */
		/************************************************************************/
		if (TRUE == GetConfig(TEXT("IsMove")))
		{
			wndPt.x = curPt.x + CURSOR_WINDOW_DIS_X;
			wndPt.y = curPt.y + CURSOR_WINDOW_DIS_Y;

			if(curPt.y + CURSOR_WINDOW_DIS_Y + wndRect.bottom - wndRect.top >= cyScreen)
			{
				wndPt.y = curPt.y - CURSOR_WINDOW_DIS_Y - (wndRect.bottom - wndRect.top);
			}
			if(curPt.x + CURSOR_WINDOW_DIS_X + wndRect.right - wndRect.left >= cxScreen)
			{
				wndPt.x = curPt.x - CURSOR_WINDOW_DIS_X - (wndRect.right - wndRect.left);
			}

			SetWindowPos(hDlg, NULL, wndPt.x, wndPt.y, 0, 0, SWP_NOZORDER | SWP_NOSIZE);
		}

		/************************************************************************/
		/*动态更新取色相关                                                      */
		/************************************************************************/
		hdcScreen = GetDC(NULL);
		hdcView1 = GetDC(hwndView1);
		hdcView2 = GetDC(hwndView2);
		hdcColor = GetDC(hwndColor);

		//显示当前颜色值
		clr = GetPixel(hdcScreen, curPt.x, curPt.y);
		GetClrTypeAndRGBText(GetConfig(TEXT("ColorType")), clr, szBuffer);
		SetWindowText(GetDlgItem(hDlg, IDS_COLOR), szBuffer);

		//放大镜
		catchRect.left = curPt.x - (int)((viewRect1.right - viewRect1.left)/nViewRatio1/2);
		catchRect.right = curPt.x + (int)((viewRect1.right - viewRect1.left)/nViewRatio1/2);
		catchRect.top = curPt.y - (int)((viewRect1.bottom - viewRect1.top)/nViewRatio1/2);
		catchRect.bottom = curPt.y + (int)((viewRect1.bottom - viewRect1.top)/nViewRatio1/2);
		StretchBlt(hdcView1, 0, 0, viewRect1.right-viewRect1.left, viewRect1.bottom-viewRect1.top,
				   hdcScreen, 
				   catchRect.left, catchRect.top, catchRect.right-catchRect.left, catchRect.bottom-catchRect.top, 
				   SRCCOPY);

		catchRect.left = curPt.x - (int)((viewRect2.right - viewRect2.left)/nViewRatio2/2);
		catchRect.right = curPt.x + (int)((viewRect2.right - viewRect2.left)/nViewRatio2/2);
		catchRect.top = curPt.y - (int)((viewRect2.bottom - viewRect2.top)/nViewRatio2/2);
		catchRect.bottom = curPt.y + (int)((viewRect2.bottom - viewRect2.top)/nViewRatio2/2);
		StretchBlt(hdcView2, 0, 0, viewRect2.right-viewRect2.left, viewRect2.bottom-viewRect2.top,
					hdcScreen, 
					catchRect.left, catchRect.top, catchRect.right-catchRect.left, catchRect.bottom-catchRect.top, 
					SRCCOPY);

		//十字架
		SelectObject(hdcView1, GetStockObject(BLACK_BRUSH));
		MoveToEx(hdcView1, 0, (viewRect1.bottom - viewRect1.top)/2, NULL);
		LineTo(hdcView1, viewRect1.right-viewRect1.left, (viewRect1.bottom - viewRect1.top)/2);
		MoveToEx(hdcView1, (viewRect1.right-viewRect1.left)/2, 0, NULL);
		LineTo(hdcView1, (viewRect1.right-viewRect1.left)/2, viewRect1.bottom - viewRect1.top);
		SelectObject(hdcView1, GetStockObject(WHITE_BRUSH));

		SelectObject(hdcView2, GetStockObject(BLACK_BRUSH));
		MoveToEx(hdcView2, 0, (viewRect2.bottom - viewRect2.top)/2, NULL);
		LineTo(hdcView2, viewRect2.right-viewRect2.left, (viewRect2.bottom - viewRect2.top)/2);
		MoveToEx(hdcView2, (viewRect2.right-viewRect2.left)/2, 0, NULL);
		LineTo(hdcView2, (viewRect2.right-viewRect2.left)/2, viewRect2.bottom - viewRect2.top);
		SelectObject(hdcView2, GetStockObject(WHITE_BRUSH));

		//当前颜色框
		StretchBlt(hdcColor, 0, 0, colorRect.right-colorRect.left, colorRect.bottom-colorRect.top,
				   hdcScreen, curPt.x, curPt.y, 1, 1,
				   SRCCOPY);

		ReleaseDC(hwndColor, hdcColor);
		ReleaseDC(hwndView1, hdcView1);
		ReleaseDC(hwndView2, hdcView2);
		ReleaseDC(NULL, hdcScreen);

		return (TRUE);

	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case IDCANCEL:
            {
                SendMessage(hDlg, WM_CLOSE, 0, 0);
            }
            return (TRUE);

		case IDOK:
            {
                
            }
			return (TRUE);
		}
		return (FALSE);
	}
	return (FALSE);
}

/************************************************************************/
/* 设置窗口为悬浮窗口                                                   */
/************************************************************************/
void SetWindowToSuspend(HWND hwnd)
{
	SetWindowLong(hwnd, GWL_STYLE, WS_POPUP);							//悬浮窗口无边界,一旦附加WS_VISVIBLE窗口立即显示
	SetWindowLong(hwnd, GWL_EXSTYLE, WS_EX_TOPMOST | WS_EX_LAYERED);	//设置悬浮窗口为所有窗口顶端并且为分层窗口
	SetLayeredWindowAttributes(hwnd, 0, 255, LWA_ALPHA);				//设置窗口半透明
}
