#ifndef SETTING_HISTORY_PROC_H_H
#define SETTING_HISTORY_PROC_H_H

#include "colorOperate.h"

BOOL CALLBACK SettingHistoryProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
void ShowCurColor(HWND hDlg, COLORREF clr);
COLORNODE GetSliderClrNode(HWND hDlg);

#endif