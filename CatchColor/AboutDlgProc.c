#include <Windows.h>
#include <commctrl.h>

#include "resource.h"

#include "AboutDlgProc.h"

BOOL CALLBACK AboutDlgProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	PNMLINK pNMLink;
	LITEM   item;

	switch(message)
	{
	case WM_CLOSE:
        {
            EndDialog(hDlg,0);
        }
		return (TRUE);

	//��������
	case WM_NOTIFY:
		switch (((LPNMHDR)lParam)->code)
		{

		case NM_CLICK:          // Fall through to the next case.
		case NM_RETURN:
			{
				pNMLink = (PNMLINK)lParam;
				item    = pNMLink->item;

				if ((((LPNMHDR)lParam)->hwndFrom == GetDlgItem(hDlg, IDB_SYSLINK)))
				{
					ShellExecute(NULL, TEXT("open"), item.szUrl, NULL, NULL, SW_SHOWNORMAL);
				}

				break;
			}
		}

		return TRUE;

	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case IDOK:
			SendMessage(hDlg, WM_CLOSE, 0, 0);
			return TRUE;
		}
		return (FALSE);
	}
	return (FALSE);
}